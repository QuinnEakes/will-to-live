﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    public DestroyerScript destroy;
    [SerializeField] bool randomSpawn = true;
    [SerializeField] Rigidbody2D tear;
    public float min = 5, max = 10;
    public float tps = 0.5f;
    public int totalTears = 0;
    void Start()
    {
        StartCoroutine("spawner");
    }

    public IEnumerator spawner()
    {
        while(gameObject.activeInHierarchy)
        {
            Vector3 spawnPos = transform.position + new Vector3(Random.Range(min, max), Random.Range(min, max), 0);
            yield return new WaitForSeconds(tps);
            Rigidbody2D clone;

            clone = Instantiate(tear, spawnPos, transform.rotation);
            clone.velocity = Vector2.down * 2;
            totalTears++;
            
        }
        
    }
}
