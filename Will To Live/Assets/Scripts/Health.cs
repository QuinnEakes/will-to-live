﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{

    public float health = 0f;




    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Projectile" || other.gameObject.tag == "Boss")
        {
            TakeDamage();
        }
    }

    void TakeDamage()
    {
        health -= 1;
        if (health <= 0)
        {
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        

        if(health == 0) 
         {
            Destroy(gameObject);
         }  
        


    }
}
