﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoulBoss : MonoBehaviour
{

    public Transform target;
    public float health = 1f;

    public float speed = 1f;

    private float time = 0.0f;
    public float interpolationPeriod = 0.1f;

    AudioSource m_audio;

    // Start is called before the first frame update
    void Start()
    {
        m_audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Weapon")
        {
            TakeDamage();
        }
    }

    void TakeDamage()
    {
        health -= 1;
        if (health <= 0)
        {
            Destroy(gameObject);
            SceneManager.LoadScene("Level 3");
        }

    }


    void Update()
    {

        transform.position += (transform.up * speed);


        time += Time.deltaTime;

        if (time >= interpolationPeriod)
        {
            time = time - interpolationPeriod;
            transform.up = target.position - transform.position;
            m_audio.Play(); 

        }
    }
}
